export const renderGlassesList = function (data) {
  let image_list = "";
  data.forEach((item) => {
    image_list += `<img onClick="displayGlasses('${item.id}')" src="${item.src}" id="glasses" 
    class="col-4">`;
  });

  document.getElementById("vglassesList").innerHTML = image_list;
};
